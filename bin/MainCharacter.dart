import 'Obj.dart';
import 'Map.dart';

class MainCharacter extends Obj {
  late Map map;
  late int power;
  MainCharacter(String symbol, int x, int y, Map map, int power)
      : super(symbol, x, y) {
    this.map = map;
    this.power = power;
  }

  get getPower => this.power;

  set setPower(power) => this.power = power;

  bool canWalk(int x, int y) {
    return power > 0 && map.inMap(x, y) && !map.isTree(x, y);
  }

  void reducePower() {
    power--;
  }

  bool walkW() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    reducePower();
    return false;
  }

  bool walkE() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    reducePower();
    return false;
  }

  bool walkS() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    reducePower();
    return false;
  }

  bool walkN() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    reducePower();
    return false;
  }

  bool walk(String direction) {
    switch (direction) {
      case 'w':
        if (walkN()) return false;
        break;
      case 's':
        if (walkS()) return false;
        break;
      case 'd':
        if (walkE()) return false;
        break;
      case 'a':
        if (walkW()) return false;
        break;
      default:
        return false;
    }
    return true;
  }

  @override
  String toString() {
    return "MainCharacter >> " +
        "x = $x y = $y Symbol : $symbol" +
        " Power : $power";
  }
}
