import 'dart:io';

import 'package:dart_project/dart_project.dart' as dart_project;
import 'Exit.dart';
import 'Map.dart';
import 'Monster.dart';
import 'Obj.dart';
import 'MainCharacter.dart';
import 'Tree.dart';

void main() {
  Map map = Map(5, 20);
  MainCharacter mc = MainCharacter("C", 0, 0, map, 21);

  Monster monster1 = Monster("M", 1, 5, 10);
  Monster monster2 = Monster("M", 2, 18, 10);
  Monster monster3 = Monster("M", 3, 11, 10);
  Exit exit = Exit(4, 9);
  //set obj into map
  map.setMainCharacer(mc);
  map.addObj(Tree(2, 1));
  map.addObj(Tree(2, 13));
  map.addObj(Tree(3, 15));
  map.addObj(Tree(3, 6));
  map.addObj(Tree(3, 9));
  map.addObj(Tree(1, 9));
  map.addObj(Tree(4, 17));
  map.addObj(monster1);
  map.addObj(monster2);
  map.addObj(monster3);
  map.addObj(exit);

  //loop game
  while (true) {
    map.showMap();
    String direction = stdin.readLineSync()!;
    if (direction == 'q') {
      break;
    }
    mc.walk(direction);
    checkMonster(map, mc);
    if (gameFinish(map, mc)) {
      print("You Win !!");
      break;
    }
  }
}

void checkMonster(Map map, MainCharacter mc) {
  if (map.isMonster(mc.getX, mc.getY)) {
    if (map.checkStat()) {
      map.delMonster(mc.getX, mc.getY);
    } else {
      print("Not enough power to defeat a monster");
    }
  }
}

bool gameFinish(Map map, MainCharacter mc) {
  if (map.monsterClear() && map.isExit(mc.getX, mc.getY)) {
    return true;
  }
  return false;
}
