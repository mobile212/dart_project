import 'dart:io';
import 'dart:mirrors';

import 'Exit.dart';
import 'MainCharacter.dart';
import 'Monster.dart';
import 'Obj.dart';
import 'Tree.dart';

class Map {
  late int width;
  late int height;
  late MainCharacter mc;
  List<Obj> object = [];
  int objCount = 0;

  Map(int width, int height) {
    this.width = width;
    this.height = height;
  }

  void addObj(Obj obj) {
    object.add(obj);
    objCount++;
  }

  void delMonster(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (object[o] is Monster && object[o].isOn(x, y)) {
        object.remove(object[o]);
        objCount--;
        mc.setPower = mc.power + 4;
      }
    }
  }

  bool checkStat() {
    for (int o = 0; o < objCount; o++) {
      if (object[o] is Monster) {
        Monster monster = object[o] as Monster;
        checkBoss(monster);
        if (mc.getPower > monster.getPower) {
          return true;
        }
      }
    }
    return false;
  }

  void checkBoss(Monster monster) {
    if (monster.getSymbol == 'B') {
      monster.setPower = 20;
    } else {
      monster.setPower = 10;
    }
  }

  bool monsterClear() {
    for (int o = 0; o < objCount; o++) {
      if (object[o] is Monster) {
        return false;
      }
    }
    return true;
  }

  void setMainCharacer(MainCharacter mc) {
    this.mc = mc;
    addObj(mc);
  }

  void printSymbol(int x, int y) {
    String symbol = '-';
    for (int i = 0; i < objCount; i++) {
      if (object[i].isOn(x, y)) {
        symbol = object[i].getSymbol;
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    print(mc);
    for (int x = 0; x < width; x++) {
      for (int y = 0; y < height; y++) {
        printSymbol(x, y);
      }
      print("");
    }
  }

  void showMc() {
    print(mc.getSymbol());
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isTree(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (object[o] is Tree && object[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  bool isExit(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (object[o] is Exit && object[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  bool isMonster(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (object[o] is Monster && object[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }
}
