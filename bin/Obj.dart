import 'dart:mirrors';

class Obj {
  late int x;
  late int y;
  late String symbol;

  Obj(String symbol, int x, int y) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }


  get getX => this.x;

  set setX(x) => this.x = x;

  get getY => this.y;

  set setY(y) => this.y = y;

  get getSymbol => this.symbol;

  set setSymbol(symbol) => this.symbol = symbol;

  @override
  String toString() {
    return "Obj {" "x= " "$x " "y= " "$y " ", symbol= " "$symbol }";
  }
}
