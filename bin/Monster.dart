import 'Obj.dart';

class Monster extends Obj {
  late int power;
  Monster(String symbol, int x, int y, int power) : super(symbol, x, y);

  get getPower => this.power;

  set setPower(power) => this.power = power;
}
